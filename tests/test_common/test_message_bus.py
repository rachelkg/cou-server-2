from cou_server.common.message_bus import MessageBus, subscribe


def test_subscribe():
    """ Verify that functions can subscribe to various events that are published
        on the bus.
    """

    test_data = {"foo": "bar"}
    data_caught = False

    @subscribe("test_channel")
    def test_handler(data):
        assert data == test_data
        nonlocal data_caught
        data_caught = True

    MessageBus().publish("test_channel", test_data)
    assert data_caught
