# `cou_server`

> Children of Ur's Python-based web server

This repository contains the source code for Children of Ur's Python-based web server.

[![pipeline status](https://gitlab.com/childrenofur/cou-server-2/badges/master/pipeline.svg)](https://gitlab.com/childrenofur/cou-server-2/commits/master)
[![coverage report](https://gitlab.com/childrenofur/cou-server-2/badges/master/coverage.svg)](https://gitlab.com/childrenofur/cou-server-2/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## License

Children of Ur is based on Tiny Speck's browser-based game, Glitch&trade;. The original game's elements have been released into the public domain.
For more information on the original game and its licensing information, visit <a href="http://www.glitchthegame.com" target="_blank">glitchthegame.com</a>.

License information for other assets used in Children of Ur can be found in `ATTRIBUTION.md`.

## Usage

TODO

## Contributing

`cou_server` is written in [Python](https://www.python.org/), so the first thing you'll need to do (if you haven't already) is to install it.

### Set up a development environment

TODO
