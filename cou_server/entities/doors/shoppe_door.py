from cou_server.entities.spritesheet import SpriteSheet
from .door import Door


INTERIOR = SpriteSheet(
    state_name="door_shoppe_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_shoppeIn.svg",
    sheet_width=187,
    sheet_height=164,
    frame_width=187,
    frame_height=164,
    num_frames=1,
    loops=True,
)

EXTERIOR = SpriteSheet(
    state_name="door_shoppe_ext",
    url="https://childrenofur.com/assets/entityImages/door_asset_shoppe.svg",
    sheet_width=182,
    sheet_height=166,
    frame_width=182,
    frame_height=166,
    num_frames=1,
    loops=True,
)


class ShoppeDoor(Door):
    def __init__(self, entity_id, street_name, x, y):
        self.outside = street_name != "Uncle Friendly's Emporium"

        if self.outside:
            # Enter Uncle Friendly's Emporium
            self.to_location = "LM4118MEHFDMM"
            self.current_state = EXTERIOR
        elif street_name == "Guillermo Gamera Way":
            # Exit to Guillermo Gamera Way
            self.to_location = "LM4109NI2R640"
            self.current_state = INTERIOR

        self.type = "Uncle Friendly's Emporium"
        super().__init__(entity_id, street_name, x, y)
