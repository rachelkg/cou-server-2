from datetime import datetime, timedelta
from random import randint

from cou_server.common.geometry.point import Point
from cou_server.common.geometry.rectangle import Rectangle
from cou_server.entities.entity import Entity, SpriteSheet
from cou_server.entities.action import Action

# 1px x 1px transparent gif.
# The client will disable interaction on this state by checking the url string,
# so update it in the client as well as the server if you change it.
TRANSPARENT_SPRITE = SpriteSheet(
    state_name="_hidden",
    url="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
    sheet_width=1,
    sheet_height=1,
    frame_width=1,
    frame_height=1,
    num_frames=1,
    loops=True,
)

UPDATE_FPS = 12

pending_bubble_callbacks = dict()


# The actions map key string should be equivalent to the name of a function
# as it will be dynamically called in street_update_handler when the client
# attempts to perform one of the available actions;


class NPC(Entity):
    can_rename: bool = False
    metadata: dict = dict()
    facing_right: bool = True
    grounded: bool = False
    name_override: str = None
    prev_x: float = None
    prev_y: float = None
    speed: float = 0
    y_speed: float = 0
    y_accel: float = -2400
    _collisions_rect: Rectangle
    cannot_flip: bool = False

    # Username and chat bubble text
    personal_bubbles: dict = dict()

    pending_button_callcallbacks = {}

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__()

        self.entity_id = entity_id
        self.x = x
        self.y = y
        self.z = z
        self.rotation = rotation
        self.h_flip = h_flip
        self.street_name = street_name
        self.respawn = datetime.utcnow()

    def customize_actions(self, email: str) -> list:
        custom_actions = list()

        if self.can_rename:
            # TODO: await SkillManager.getLevel('animal_kinship', email) >= 5
            custom_actions += Action("rename")

        return custom_actions

    def restore_state(self, map_data: dict):
        self.metadata = map_data

        if self.metadata["facingRight"] == "false":
            self.facing_right = False

        if "nameOverride" in self.metadata:
            self.name_override = self.metadata["nameOverride"]

    def get_persist_metadata(self) -> dict:
        metadata = {"facingRight": "true" if self.facing_right else "false"}

        if self.can_rename and self.name_override:
            metadata["nameOverride"] = self.name_override

        return metadata

    @property
    def width(self) -> int:
        return self.current_state.frame_width

    @property
    def height(self) -> int:
        return self.current_state.frame_height

    @property
    def has_moved(self) -> bool:
        return self.x != self.prev_x or self.y != self.prev_y

    @property
    def street(self):
        # TODO: StreetUpdateHandler.streets[streetName]
        return None

    @property
    def collisions_rect(self) -> Rectangle:
        if self._collisions_rect is None:
            top_left = Point(self.x, self.y)
            bottom_right = Point(self.x + self.width, self.y + self.height)
            self._collisions_rect = Rectangle(top_left, bottom_right)
        else:
            self._collisions_rect.left = self.x
            self._collisions_rect.top = self.y
            self._collisions_rect.width = self.width

        return self._collisions_rect.clone()

    def update(self, simulate_tick: bool = False) -> None:
        """
        Update the state and position of the NPC.
        Will be called UPDATE_FPS times per second.
        :param simulate_tick: If you want do something more expensive,
        probably it should only be done when this is true
        """
        self.prev_x = self.x
        self.prev_y = self.y

    def turn_around(self):
        self.facing_right = not self.facing_right

    def default_wall_action(self, wall=None):
        self.turn_around()

        if wall is None:
            return

        if self.facing_right:
            if self.collisions_rect.right >= wall.bounds.left:
                self.x = wall.bounds.left - self.width - 1
        else:
            if self.collisions_rect.left < wall.bounds.left:
                self.x = wall.bounds.right + 1

    def default_ledge_action(self):
        self.x = self.prev_x
        self.y = self.prev_y
        self.turn_around()

    def default_x_action(self):
        self.x += self.speed * (1 if self.facing_right else -1) / UPDATE_FPS

    def default_y_action(self):
        self.y_speed -= self.y_accel / UPDATE_FPS
        self.y += self.y_speed / UPDATE_FPS
        # TODO: fall to ground
        # self.y = self.street.get_ground_y(self.x, self.prev_x, self.width, self.height)

    def move_xy(
        self, x_action=None, y_action=None, wall_action=None, ledge_action=None
    ):
        if self.prev_y is None:
            raise ValueError("Did you forget to call super().update()?")

        y_action = y_action or self.default_y_action
        x_action = x_action or self.default_x_action
        wall_action = wall_action or self.default_wall_action
        ledge_action = ledge_action or self.default_ledge_action

        x_action()
        y_action()

        # If our new y value is more than 10 pixels away from the old one
        # we probably changed platforms (dropped down) so decide what to do about that
        if self.grounded and abs(self.y - self.prev_y) > 10:
            ledge_action()
        elif abs(self.y - self.prev_y) < 10:
            self.grounded = True

        # Stop walking into walls, take an action if we're colliding with one
        for wall in self.street.walls:
            if self.collisions_rect.intersects(wall.bounds):
                wall_action(wall)

        # Treat the sides of the street as walls too
        if self.x < 0:
            wall_action()

        if (
            self.street.bounds is not None
            and self.x > self.street.bounds.width - self.width
        ):
            wall_action()
            self.x = self.street.bounds.width - self.width

    def say(self, message: str = None, to_username: str = None, buttons: dict = None):
        message = (message or str()).strip()

        if not buttons:
            # No interaction needed, use a normal bubble
            now = datetime.utcnow()
            if self.say_timeout is None or self.say_timeout - now < 0:
                if not to_username:
                    self.bubble_text = message
                else:
                    self.personal_bubbles[to_username] = message

                ttl = len(message) * 30 + 3000  # Minimum 3s plus 0.3s per character
                ttl = min(ttl, 10000)  # Max 10s

                message_duration = timedelta(milliseconds=ttl)
                self.say_timeout = now + message_duration

                # TODO: remove bubble after timer
                # new Timer(messageDuration, () {
                # 					if (toUsername == null) {
                # 						bubbleText = null;
                # 					} else {
                # 						personalBubbles[toUsername] = null;
                # 					}
                # 					resetGains();
                # 				});
        else:
            # Message format:
            # message|||id1,text1|id2,text2
            message += "|||"

            # Add buttons to message
            for name, callback in buttons:
                btn_id = randint(0, 999_999)
                message += f"{btn_id},{name}|"

                # Create handler
                def handler():
                    # Perform callback
                    callback()

                    # Close bubble
                    if not to_username:
                        self.bubble_text = None
                    else:
                        del self.personal_bubbles[to_username]

                    # TODO: self.reset_gains()

                    # Remove handler
                    del pending_bubble_callbacks[btn_id]

                # Register handler
                pending_bubble_callbacks[btn_id] = handler

            # Remove trailing pipes
            if message.endswith("|"):
                message = message[0:-1]

            # Send buttons to client and wait for a response
            if to_username:
                self.personal_bubbles[to_username] = message
            else:
                self.bubble_text = message

    def __dict__(self, username: str = None):
        entity = super().__dict__ + {
            "id": self.id,
            "url": self.current_state.url,
            "type": self.type,
            "nameOverride": self.name_override,
            "numRows": self.current_state.num_rows,
            "numColumns": self.current_state.num_cols,
            "numFrames": self.current_state.num_frames,
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "rotation": self.rotation,
            "h_flip": self.h_flip,
            "dontFlip": self.cannot_flip,
            "speed": self.speed,
            "ySpeed": self.y_speed,
            "animation_name": self.current_state.state_name,
            "width": self.width,
            "height": self.height,
            "loops": self.current_state.loops,
            "loopDelay": self.current_state.loop_delay,
            "facingRight": self.facing_right,
            "actions": [action.__dict__ for action in self.actions],
        }

        if username:
            # Customize bubble text for the player
            entity["bubbleText"] = self.personal_bubbles[username] or self.bubble_text

        return entity
