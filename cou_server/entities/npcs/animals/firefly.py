from datetime import datetime, timedelta
from random import randint

from cou_server.common import get_logger
from cou_server.common.util import toast
from cou_server.entities.action import ItemRequirements, EnergyRequirements, Action
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

logger = get_logger(__name__)

REQ_ITEM = ItemRequirements()
REQ_ITEM.any = ["firefly_jar"]
REQ_ITEM.error = "Fireflies won't stay in your hands. You need a jar."

REQ_ENERGY = EnergyRequirements(3)
REQ_ENERGY.error = "Chasing fireflies is hard work, so you'll need at least 3 energy."

COLLECT = Action("collect", action_word="chasing")
COLLECT.item_requirements = REQ_ITEM
COLLECT.energy_requirements = REQ_ENERGY


STATES = {
    "fullPath": SpriteSheet(
        "fullPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_fullPath_png_1354833043.png",
        870,
        360,
        87,
        40,
        89,
        True,
    ),
    "halfPath": SpriteSheet(
        "halfPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_halfPath_png_1354833044.png",
        870,
        160,
        87,
        40,
        40,
        True,
    ),
    "smallPath": SpriteSheet(
        "smallPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_smallPath_png_1354833044.png",
        870,
        80,
        87,
        40,
        20,
        True,
    ),
}


class Firefly(NPC):
    # TODO:
    #  new Clock()
    clock = None

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.type = "Firefly"
        self.action_time = 4000
        self.actions += [COLLECT]
        self.speed = 5  # px/s
        self.states = STATES
        self.set_state("fullPath")

    def collect(self, user_socket, email: str = None) -> bool:
        adding = randint(1, 4)
        skipped = adding

        try:
            # TODO: InventoryV2.addFireflyToJar(email, userSocket, amount: adding)
            skipped = 0
        except:
            logger.warning(f"Could not collect fireflies for <email={email}>")

        added = adding - skipped

        if added == 0:
            toast("You don't have any room in your jar!", user_socket)
            return False
        else:
            toast(
                f"You caught {added} firefl{'y' if added == 1 else 'ies'}", user_socket
            )

            # TODO: trySetMetabolics
            #  (email, energy: -COLLECT.energy_requirements.energy_amount)

            # Small flight path for 10 seconds
            self.set_state("smallPath")

            return True

    def update(self, simulate_tick: bool = False):
        super().update(simulate_tick)

        am = "am" in self.clock.time
        hour = int(self.clock.time.split(":")[0])
        minute = int(self.clock.time.split(":")[1][0:2])

        if (not am or hour >= 6) and (am or hour <= 8 or minute < 30):
            # Firefly time is 8:30 PM to 6:00 AM, not now
            return

        # If respawn is in the past, it is time to choose a new animation
        if self.respawn and datetime.utcnow() > self.respawn:
            # 50% chance to move the other way...gradually
            if randint(0, 1) == 0:
                self.turn_around()

            path_size = randint(0, 4)
            if 0 <= path_size <= 1:
                self.set_state("fullPath")
            elif 2 <= path_size <= 3:
                self.set_state("halfPath")
            elif path_size == 4:
                self.set_state("smallPath")

            # Stay for 10 seconds
            length = 10000 * (self.current_state.num_frames / 30 * 1000) // 1
            self.respawn = datetime.utcnow() + timedelta(milliseconds=length)
