class SpriteSheet:
    def __init__(
        self,
        state_name: str,
        url: str,
        sheet_width: int,
        sheet_height: int,
        frame_width: int,
        frame_height: int,
        num_frames: int,
        loops: bool,
        loop_delay: int = 0,
    ):
        self.state_name = state_name
        self.url = url
        self.sheet_width = sheet_width
        self.sheet_height = sheet_height
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.num_frames = num_frames
        self.loops = loops
        self.loop_delay = loop_delay

        self.num_rows = self.sheet_height // self.frame_height
        self.num_cols = self.sheet_width // self.frame_width

    def __dict__(self):
        return {
            "stateName": self.state_name,
            "url": self.url,
            "sheetWidth": self.sheet_width,
            "sheetHeight": self.sheet_height,
            "frameWidth": self.frame_width,
            "frameHeight": self.frame_height,
            "numFrames": self.num_frames,
            "numRows": self.num_rows,
            "numColumns": self.num_cols,
            "loops": self.loops,
            "loopDelay": self.loop_delay,
        }

    def __str__(self):
        return str(self.__dict__)
