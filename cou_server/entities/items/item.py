from copy import deepcopy
from random import randint

from cou_server.entities.entity import create_id
from cou_server.entities.action import Action


# All items known to the server
ITEMS = dict()

# Discounts, stored as itemType: part paid out of 1 (eg. 0.8 for 20% off)
DISCOUNTS = {}

DROP = Action("drop")
DROP.description = "Drop this item on the ground."
DROP.multi_enabled = True

PICK_UP = Action("pickup")
PICK_UP.description = "Put this item in your inventory."
PICK_UP.multi_enabled = True


class Item:
    category: str
    icon_url: str
    sprite_url: str
    broken_url: str
    tool_animation: str
    name: str
    recipe_name: str
    description: str
    item_type: str
    item_id: str

    price: int
    stacks_to: int
    icon_num: int = 4
    durability: int
    sub_slots: int = 0

    x: float
    y: float

    on_ground: bool = False
    is_container: bool = False

    sub_slot_filter: list
    actions: list

    consume_values: dict
    metadata: dict

    drop: Action(DROP)
    pick_up: Action(PICK_UP)


    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def put_item_on_ground(self, x, y, label, id=None):
        pass

    @property
    def discount(self) -> float:
        if self.item_type in DISCOUNTS:
            return DISCOUNTS[self.item_type]

        return 1.0

    @staticmethod
    def clone(item_type: str):
        return deepcopy(ITEMS[item_type])

    @property
    def action_list(self) -> list:
        result = list()

        if self.on_ground:
            for action in self.actions:
                if action.ground_action:
                    result += action.__dict__
            result += PICK_UP.__dict__
        else:
            # Make sure the drop action is last
            drop_instance = None
            for action in self.actions:
                if action.action_name == DROP.action_name:
                    drop_instance = action
                    break
            if drop_instance:
                self.actions.remove(drop_instance)

            result = [action.__dict__ for action in self.actions]
            result += DROP.__dict__

        return result

    def filter_allows(self, test_item, item_type):
        # Allow an empty slot
        if not test_item and not item_type:
            return True

        # Bags except empty item types (this is an empty slot)
        if item_type is not None and len(item_type) == 0:
            return True

        if not test_item:
            test_item = ITEMS[item_type]

        if len(self.sub_slot_filter) == 0:
            return not test_item.is_container
        else:
            return test_item.item_type in self.sub_slot_filter

    def __repr__(self):
        return f"An item of type {self.item_type} with metadata {self.metadata}"

    def put_on_ground(self, x, y, street_name: str, item_id: str = None, count: int = 1):
        street = None  # TODO: StreetUpdateHandler.streets[streetName]
        if not street:
            return

        for i in range(0, count):
            temp_id = item_id
            if not temp_id:
                rand_str = str(randint(0, 9999))
                temp_id = "i" + create_id(x, y, self.item_type, street_name + rand_str)

            item = Item.clone(self.item_type)
            item.x = x
            item.y = y # TODO: street.getYFromGround(item.x, item.y, 1, 1)
            item.item_id = temp_id
            item.on_ground = True
            item.metadata = self.metadata
            # TODO: street.groundItems[tempId] = item;
