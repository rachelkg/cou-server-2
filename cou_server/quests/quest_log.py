import json
from threading import Timer

from marshmallow import fields
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import backref, relationship, reconstructor

from cou_server.common.database import Base
from cou_server.common.message_bus import MessageBus, subscribe
from cou_server.endpoints import quest as quest_service
from cou_server.models.user import UserSchema
from cou_server.quests.quest import Quest, QuestSchema
from cou_server.quests.trackable import Trackable


class QuestLog(Base, Trackable):
    __tablename__ = "user_quests"

    id = Column(Integer, primary_key=True)
    completed_list = Column(String, default="[]")
    in_progress_list = Column(String, default="[]")

    # relationships to other tables
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", backref=backref("quest_log", uselist=False))

    def __init__(self, **kwargs):
        self.completed_list = "[]"
        self.in_progress_list = "[]"

        for prop in kwargs:
            setattr(self, prop, kwargs[prop])

        self.do_load()

    @reconstructor
    def do_load(self):
        self.offering_quest = False
        Trackable.__init__(self)

    def __repr__(self):
        return f"QuestLog: {QuestLogSchema().dump(self)}"

    @property
    def completed_quests(self):
        if not hasattr(self, "_completed_quests"):
            self._completed_quests = QuestSchema().loads(self.completed_list, many=True)
        return self._completed_quests

    @completed_quests.setter
    def completed_quests(self, new_list):
        self.completed_list = QuestSchema().dumps(new_list, many=True)
        self._completed_quests = new_list

    @property
    def in_progress_quests(self):
        if not hasattr(self, "_in_progress_quests"):
            self._in_progress_quests = QuestSchema().loads(
                self.in_progress_list, many=True
            )
        return self._in_progress_quests

    @in_progress_quests.setter
    def in_progress_quests(self, new_list):
        self.in_progress_list = QuestSchema().dumps(new_list, many=True)
        self._in_progress_quests = new_list

    def handle_complete_quest(self, data):
        if data.email != self.email:
            return

        data.quest.complete = True
        data.quest.stop_tracking()
        quest_complete = {
            "questComplete": True,
            "quest": QuestSchema().dump(data.quest),
        }
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            user_socket.send(json.dumps(quest_complete))

        if data.quest in self.in_progress_quests:
            self.in_progress_quests.remove(data.quest)
        if data.quest not in self.completed_quests:
            self.completed_quests.append(data.quest)

        quest_service.update_quest_log(self)

        # if the completed the sammich quest, go get a snocone
        # wait for a minute before offering
        if data.quest.id == "Q1":

            def offer_snocone():
                self.offer_quest("Q7")

            Timer(60, offer_snocone)

    def handle_fail_quest(self, data):
        if data.email != self.email:
            return

        data.quest.stop_tracking()

        quest_fail = {"questFail": True, "quest": QuestSchema().dump(data.quest)}
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            user_socket.send(json.dumps(quest_fail))

        if data.quest in self.in_progress_quests:
            self.in_progress_quests.remove(data.quest)

        quest_service.update_quest_log(self)

    def handle_requirement_updated(self, data):
        if data.email != self.email:
            return

        quest_service.update_quest_log(self)

    def _stop_accept_reject_subscriptions(self):
        """ Since the accept and reject are only temporary and once the user
            decides, we should stop listening for more (until next time) so that
            we aren't double, triple, etc. notified.
        """

        try:
            for channel in ["accept_quest", "reject_quest"]:
                subscription = next(
                    iter([s for s in self.mb_subscriptions if s.channel == channel]),
                    None,
                )
                MessageBus().unsubscribe(subscription)
                self.mb_subscriptions.remove(subscription)
        except:
            # best effort
            pass

    def handle_accept_quest(self, data, headers=None):
        if data.email != self.email:
            return

        if headers and headers.get("fromItem", None) == False:
            # TODO: take item from user at headers['slot'], headers['subSlot'], qty = 1
            pass

        self._stop_accept_reject_subscriptions()
        self.offering_quest = False
        self.add_in_progress_quest(data.quest_id)

    def handle_reject_quest(self, data):
        if data.email != self.email:
            return

        self._stop_accept_reject_subscriptions()
        self.offering_quest = False

    def start_tracking(self, email):
        super().start_tracking(email)

        # listen for quest completion events
        # if they don't belong to us, let someone else get them
        # if they do belong to us, send a message to the client to tell them of their success
        self.mb_subscriptions.append(
            subscribe("complete_quest", method=self.handle_complete_quest)
        )
        self.mb_subscriptions.append(
            subscribe("fail_quest", method=self.handle_fail_quest)
        )
        self.mb_subscriptions.append(
            subscribe("requirement_updated", method=self.handle_requirement_updated)
        )

        # start tracking on all our in progress quests
        for quest in self.in_progress_quests:
            quest.start_tracking(email)

    def stop_tracking(self):
        super().stop_tracking()
        for quest in self.in_progress_quests:
            quest.stop_tracking()
        quest_service.update_quest_log(self)

    def add_in_progress_quest(self, quest_id):
        quest_to_add = Quest.clone(quest_id)
        if self._doing_or_done(quest_to_add):
            return False

        quest_to_add.start_tracking(self.email, just_started=True)
        self.in_progress_quests.append(quest_to_add)
        quest_service.update_quest_log(self)

        return True

    def _doing_or_done(self, quest):
        if quest is None:
            return True
        if quest in self.completed_quests or quest in self.in_progress_quests:
            return True
        return False

    def offer_quest(self, quest_id, from_item=False, slot=-1, sub_slot=-1):
        quest_to_offer = Quest.clone(quest_id)

        if self._doing_or_done(quest_to_offer) or self.offering_quest:
            return

        # check if prerequisite quests are complete
        for prereq in quest_to_offer.prerequisites:
            previous_quest = Quest.clone(prereq)
            if previous_quest not in self.completed_quests:
                return

        headers = {"fromItem": from_item, "slot": slot, "subSlot": sub_slot}
        self.mb_subscriptions.append(
            subscribe("accept_quest", method=self.handle_accept_quest, headers=headers)
        )
        self.mb_subscriptions.append(
            subscribe("reject_quest", method=self.handle_reject_quest)
        )

        quest_offer = {"questOffer": True, "quest": QuestSchema().dump(quest_to_offer)}
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            user_socket.send(json.dumps(quest_offer))
        self.offering_quest = True


class QuestLogSchema(ModelSchema):
    in_progress_quests = fields.List(fields.Nested(QuestSchema))
    completed_quests = fields.List(fields.Nested(QuestSchema))
    user = fields.Nested(UserSchema)

    class Meta:
        model = QuestLog
