from cou_server.common.message_bus import MessageBus


class Trackable:
    def __init__(self):
        self.email = None
        self.being_tracked = False
        self.timer = None
        self.mb_subscriptions = []
        self.headers = {}

    def start_tracking(self, email):
        self.email = email
        self.being_tracked = True

    def stop_tracking(self):
        if self.timer:
            self.timer.cancel()
        for subscription in self.mb_subscriptions:
            MessageBus().unsubscribe(subscription)
        self.mb_subscriptions = []
        self.being_tracked = False
