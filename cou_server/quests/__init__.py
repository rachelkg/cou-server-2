from marshmallow import Schema, post_dump


class NotNullSchema(Schema):
    @post_dump
    def remove_null_values(self, data, **kwargs):
        return {key: value for key, value in data.items() if value is not None}
