from gevent import monkey

monkey.patch_all()

import sys

from flask import Flask, Response
from flask_cors import CORS
from flask_sockets import Sockets
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from werkzeug.exceptions import NotFound

from cou_server.common import get_logger
from cou_server.common.constants import const_blueprint
from cou_server.common.database import db_create
from cou_server.common.file_cache import FileCache
from cou_server.endpoints.buffs import buffs_endpoint
from cou_server.endpoints.chat import chat_blueprint
from cou_server.endpoints.entities import entity_blueprint
from cou_server.endpoints.evelavtion import elevation_endpoint
from cou_server.endpoints.friends import friends_blueprint
from cou_server.endpoints.images import images_blueprint
from cou_server.endpoints.items import items_endpoint, load_items
from cou_server.endpoints.letters import letters_blueprint
from cou_server.endpoints.mapdata import map_blueprint, read_map_data
from cou_server.endpoints.metabolics import metabolics_endpoint, metabolics_ws_blueprint
from cou_server.endpoints.player_update import player_blueprint
from cou_server.endpoints.quest import (
    quest_blueprint,
    quests_blueprint,
    quests_ws_blueprint,
    load_quests,
)
from cou_server.endpoints.street_update import street_blueprint, street_ws_blueprint
from cou_server.endpoints.user import users_blueprint
from cou_server.endpoints.usernamecolors import usernamecolors_blueprint
from cou_server.endpoints.weather import weather_ws_blueprint
from cou_server.skills.skill_manager import skills_blueprint, load_skills

logger = get_logger(__name__)

app = Flask(__name__)
CORS(app, automatic_options=True)
app.register_blueprint(buffs_endpoint)
app.register_blueprint(const_blueprint)
app.register_blueprint(entity_blueprint)
app.register_blueprint(elevation_endpoint)
app.register_blueprint(friends_blueprint)
app.register_blueprint(images_blueprint)
app.register_blueprint(items_endpoint)
app.register_blueprint(letters_blueprint)
app.register_blueprint(map_blueprint)
app.register_blueprint(metabolics_endpoint)
app.register_blueprint(quest_blueprint)
app.register_blueprint(quests_blueprint)
app.register_blueprint(skills_blueprint)
app.register_blueprint(street_blueprint)
app.register_blueprint(users_blueprint)
app.register_blueprint(usernamecolors_blueprint)

sockets = Sockets(app)
sockets.register_blueprint(chat_blueprint)
sockets.register_blueprint(quests_ws_blueprint)
sockets.register_blueprint(metabolics_ws_blueprint)
sockets.register_blueprint(player_blueprint)
sockets.register_blueprint(street_ws_blueprint)
sockets.register_blueprint(weather_ws_blueprint)


def main():
    db_create()
    read_map_data()
    load_quests()
    load_items()
    FileCache.load_caches()
    load_skills()
    server = WSGIServer(("0.0.0.0", 8000), app, handler_class=WebSocketHandler)
    logger.info(f"Game server on port {server.server_port}")
    server.serve_forever()


if __name__ == "__main__":
    sys.exit(main())
