import inspect


class _BusSubscriber:
    def __init__(self, channel, callback, headers=None):
        self.channel = channel
        self.callback = callback
        self.headers = headers


class MessageBus:
    _instance = None

    def __new__(cls):
        if MessageBus._instance is None:
            MessageBus._instance = super().__new__(cls)
        MessageBus._initialized = False
        return MessageBus._instance

    def __init__(self):
        if self._initialized:
            return
        self.subscribers = []
        self._initialized = True

    def publish(self, channel, data, *args, **kwargs):
        for subscriber in self.subscribers:
            if subscriber.channel != channel:
                continue

            sig = inspect.signature(subscriber.callback)
            added_headers = False
            for param in sig.parameters.values():
                if (
                    param.name == "headers"
                    and param.kind == inspect.Parameter.KEYWORD_ONLY
                ):
                    added_headers = True
                    subscriber.callback(
                        data, headers=subscriber.headers, *args, **kwargs
                    )
            if not added_headers:
                subscriber.callback(data, *args, **kwargs)

    def unsubscribe(self, subscriber):
        """ Unregister a given subscriber. They won't receive any more messages """

        try:
            self.subscribers.remove(subscriber)
        except:
            # guess they're not here anymore
            pass


def subscribe(channel, method=None, headers=None):
    """ Register this function as a callback on the given channel of the
        Message Bus
    """

    if method:
        subscriber = _BusSubscriber(channel, method, headers=headers)
        MessageBus().subscribers.append(subscriber)
        return subscriber

    def decorator(func):
        MessageBus().subscribers.append(_BusSubscriber(channel, func, headers=headers))
        return func

    return decorator
