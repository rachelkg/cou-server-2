from flask import Blueprint, jsonify
from flask_cors import CORS

const_blueprint = Blueprint("const_blueprint", __name__)
CORS(const_blueprint, automatic_options=True)

QUOIN_LIMIT = 100
QUOIN_MULTIPLIER_LIMIT = 73  # prime
CHANGE_USERNAME_COST = 1000  # currants


@const_blueprint.route("/constants/json")
def get_all_constants():
    return jsonify(
        {
            "quoinLimit": QUOIN_LIMIT,
            "quoinMultiplierLimit": QUOIN_MULTIPLIER_LIMIT,
            "changeUsernameCost": CHANGE_USERNAME_COST,
        }
    )
