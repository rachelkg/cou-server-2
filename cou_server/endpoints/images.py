import base64
import io
import json
from pathlib import Path
import re
from urllib.parse import quote_plus
import urllib.request

from flask import Blueprint, Response, jsonify, request
from flask_cors import CORS
from PIL import Image

from cou_server.common import CACHE_DIR
from cou_server.common.database import db_session_context
from cou_server.common.file_cache import FileCache
from cou_server.endpoints import chat
from cou_server.models.user import User, UserSchema

images_blueprint = Blueprint("images_blueprint", __name__)
CORS(images_blueprint, automatic_options=True)


AVATAR_IMAGE_CACHE_DIR = Path(CACHE_DIR, "avatar_images")

if not AVATAR_IMAGE_CACHE_DIR.is_dir():
    AVATAR_IMAGE_CACHE_DIR.mkdir()


@images_blueprint.route("/setCustomAvatar")
def set_custom_avatar():
    username = request.args.get("username")
    avatar = request.args.get("avatar")

    if avatar == username:
        avatar = None

    with db_session_context() as db_session:
        user = db_session.query(User).filter(User.username == username)
        user.custom_avatar = avatar

    FileCache.heads_cache.pop(username, None)
    FileCache.heads_cache.pop(f"{username}.fullheight", None)


@images_blueprint.route("/getSpritesheets")
def get_spritesheets():
    username = request.args.get("username", None)
    no_custom_avatars = request.args.get("noCustomAvatars", False)

    if not username:
        return jsonify({})

    if not no_custom_avatars:
        with db_session_context() as db_session:
            user = db_session.query(User).filter(User.username == username).first()
            if user.custom_avatar:
                username = user.custom_avatar

    cache_file = Path(AVATAR_IMAGE_CACHE_DIR, f"{username.lower()}.json")
    try:
        with open(cache_file, "r") as cache:
            return jsonify(json.loads(cache.read()))
    except:
        with open(cache_file, "w") as cache:
            spritesheets = _get_spritesheets_from_web(username)
            cache.write(json.dumps(spritesheets))
            return jsonify(spritesheets)


def _get_spritesheets_from_web(username):
    spritesheets = {}
    url = f"http://www.glitchthegame.com/friends/search/?q={quote_plus(username)}"
    req = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
    response = urllib.request.urlopen(req).read().decode("utf-8")
    regex = f'\/profiles\/(.+)\/" class="friend-name">{username}'

    match = re.search(regex, response, re.IGNORECASE)
    if match:
        tsid = match.group(1)
        req = urllib.request.Request(
            f"http://www.glitchthegame.com/profiles/{tsid}",
            headers={"User-Agent": "Mozilla/5.0"},
        )
        response = urllib.request.urlopen(req).read().decode("utf-8")
        sheets = [
            "base",
            "angry",
            "climb",
            "happy",
            "idle1",
            "idle2",
            "idle3",
            "idleSleepy",
            "jump",
            "surprise",
        ]
        for sheet in sheets:
            regex = f'"(.+{sheet}.png)"'
            spritesheets[sheet] = re.search(regex, response).group(1)

        return spritesheets
    else:
        return _get_spritesheets_from_web("Elle Lament")


@images_blueprint.route("/getActualImageHeight")
def get_actual_image_height():
    image_url = request.args.get("url")
    num_rows = int(request.args.get("numRows"))
    num_columns = int(request.args.get("numColumns"))

    if FileCache.heights_cache.get(image_url) is not None:
        return str(FileCache.heights_cache[image_url])

    req = urllib.request.Request(image_url, headers={"User-Agent": "Mozilla/5.0"})
    with urllib.request.urlopen(req) as image_file:
        img_file = io.BytesIO(image_file.read())

    image = Image.open(img_file)
    single_frame = image.crop(
        (0, 0, image.width // num_columns, image.height // num_rows)
    )
    trimmed_frame = single_frame.crop(single_frame.getbbox())
    FileCache.heights_cache[image_url] = trimmed_frame.height
    return str(trimmed_frame.height)


@images_blueprint.route("/trimImage")
def trim_image():
    username = request.args.get("username")
    no_custom_avatars = request.args.get("noCustomAvatars", False)
    full_height = request.args.get("fullHeight", False)
    cache_key = f"{username}.fullheight" if full_height else username

    if FileCache.heads_cache.get(cache_key):
        return FileCache.heads_cache[cache_key]

    spritesheet = get_spritesheets().get_json()
    image_url = spritesheet["base"]
    if not image_url:
        return ""

    req = urllib.request.Request(image_url, headers={"User-Agent": "Mozilla/5.0"})
    with urllib.request.urlopen(req) as image_file:
        img_file = io.BytesIO(image_file.read())

    image = Image.open(img_file)
    frame_width = image.width // 15
    frame_height_scl = image.height if full_height else image.height // 1.5
    image = image.crop((image.width - frame_width, 0, image.width, frame_height_scl))
    # trimmed = image.crop(image.getbbox())

    png_bytes = io.BytesIO()
    image.save(png_bytes, format="PNG")
    encoded = base64.encodebytes(png_bytes.getvalue())
    # encoded = base64.encodebytes(image.tobytes())
    FileCache.heads_cache[cache_key] = encoded
    return encoded
