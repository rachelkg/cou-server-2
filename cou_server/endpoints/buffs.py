from flask import Blueprint, jsonify, request
from flask_cors import CORS

buffs_endpoint = Blueprint("buffs_endpoint", __name__, url_prefix="/buffs")
CORS(buffs_endpoint, automatic_options=True)


@buffs_endpoint.route("/get/<string:email>")
def buffs_get(email):
    return jsonify([])
