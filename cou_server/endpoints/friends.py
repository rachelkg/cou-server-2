from flask import Blueprint, Response, jsonify, request
from flask_cors import CORS

from werkzeug.exceptions import NotFound, BadRequest

from cou_server.api_keys import REDSTONE_TOKEN
from cou_server.common.database import db_session_context
from cou_server.common.util import toast
from cou_server.endpoints import chat, metabolics
from cou_server.models.user import User

friends_blueprint = Blueprint("friends_blueprint", __name__, url_prefix="/friends")
CORS(friends_blueprint, automatic_options=True)


@friends_blueprint.route("/add")
def add_friend():
    username = request.args.get("username")
    friend_username = request.args.get("friendUsername")
    rstoken = request.args.get("rstoken")

    if rstoken != REDSTONE_TOKEN:
        raise BadRequest("rstoken is not valid.")

    if username == friend_username:
        raise BadRequest("You cannot befriend yourself (sorry).")

    with db_session_context() as db_session:
        myself = db_session.query(User).filter(User.username == username).first()
        if not myself:
            raise BadRequest(f"Could not find a player with the name {username}.")
        friend = db_session.query(User).filter(User.username == friend_username).first()
        if not friend:
            raise BadRequest(
                f"Could not find a player with the name {friend_username}."
            )

        friends = myself.get_friends(db_session)
        if friend not in friends:
            friends.append(friend)
        myself.set_friends(db_session, friends)

        # Notify new friend (if they're online)
        if friend_username in metabolics.user_sockets:
            toast(
                f"{username} added you to their friends!",
                metabolics.user_sockets[friend_username],
                on_click=f"addFriend|{username}",
            )

    return "", 200


@friends_blueprint.route("/remove")
def remove_friend():
    username = request.args.get("username")
    friend_username = request.args.get("friendUsername")
    rstoken = request.args.get("rstoken")

    if rstoken != REDSTONE_TOKEN:
        raise BadRequest("rstoken is not valid.")

    with db_session_context() as db_session:
        myself = db_session.query(User).filter(User.username == username).first()
        if not myself:
            raise BadRequest(f"Could not find a player with the name {username}.")
        friend = db_session.query(User).filter(User.username == friend_username).first()
        if not friend:
            raise BadRequest(
                f"Could not find a player with the name {friend_username}."
            )

        friends = myself.get_friends(db_session)
        if friend in friends:
            friends.remove(friend)
        myself.set_friends(db_session, friends)

    return "", 200


@friends_blueprint.route("/list/<string:username>")
def list_friends(username):
    with db_session_context() as db_session:
        myself = db_session.query(User).filter(User.username == username).first()
        if not myself:
            myself = User(username=username, email="")
            db_session.add(myself)
            db_session.commit()
            # raise BadRequest(f'Could not find a player with the name {username}.')
        onlineUsers = []
        offlineUsers = []
        friends = myself.get_friends(db_session)
        for friend in friends:
            if friend.username in chat.users:
                onlineUsers.append(friend.username)
            else:
                offlineUsers.append(friend.username)

        statuses = {}
        for username in onlineUsers:
            statuses[username] = True
        for username in offlineUsers:
            statuses[username] = False

        return jsonify(statuses)
